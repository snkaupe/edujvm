# eduJVM

eduJVM is a simple, Java bytecode-like language, compiler and virtual machine for educational purposes. It roughly follows the example of [IJVM](https://en.wikipedia.org/wiki/IJVM), the simplified instruction set from Andrew Tanenbaum's book *Structured Computer Organization*.
