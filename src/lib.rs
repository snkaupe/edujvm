pub const CRATE_VERSION: &str = env!("CARGO_PKG_VERSION");
pub const EDUJVM_VERSION: &str = "1.0";

pub mod inspection;
pub mod compiler;

pub fn print_version() {
    println!("eduJVM Version {} (crate version {})", EDUJVM_VERSION, CRATE_VERSION);
}