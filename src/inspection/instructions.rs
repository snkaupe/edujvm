use std::fmt::Display;

use super::{util::get_u16_be, ProgramError, Result};

pub enum Instruction {
    // Stack manipulation
    BIPUSH,
    DUP,
    POP,
    SWAP,
    ILOAD,
    ISTORE,
    LDC,
    // Maths
    IADD,
    ISUB,
    IINC,
    // Logical
    IAND,
    IOR,
    // Control flow
    GOTO,
    IFEQ,
    IFNE,
    IFLT,
    IFGT,
    #[allow(non_camel_case_types)]
    IF_ICMPEQ,
    #[allow(non_camel_case_types)]
    IF_ICMPNE,
    #[allow(non_camel_case_types)]
    IF_ICMPLT,
    #[allow(non_camel_case_types)]
    IF_ICMPGE,
    #[allow(non_camel_case_types)]
    IF_ICMPGT,
    #[allow(non_camel_case_types)]
    IF_ICMPLE,
    INVOKEVIRTUAL,
    IRETURN,
    RETURN,
    // I/O
    IN,
    OUT,
    // Program control
    ERR,
    HALT,
    NOP,
}

impl Instruction {
    pub fn bytecode(&self) -> u8 {
        match self {
            Instruction::BIPUSH => 0x10,
            Instruction::DUP => 0x59,
            Instruction::POP => 0x57,
            Instruction::SWAP => 0x5F,
            Instruction::ILOAD => 0x15,
            Instruction::ISTORE => 0x36,
            Instruction::LDC => 0x12,
            Instruction::IADD => 0x60,
            Instruction::ISUB => 0x64,
            Instruction::IINC => 0x84,
            Instruction::IAND => 0x7E,
            Instruction::IOR => 0x80,
            Instruction::GOTO => 0xA7,
            Instruction::IFEQ => 0x99,
            Instruction::IFNE => 0x9A,
            Instruction::IFLT => 0x9B,
            Instruction::IFGT => 0x9D,
            Instruction::IF_ICMPEQ => 0x9F,
            Instruction::IF_ICMPNE => 0xA0,
            Instruction::IF_ICMPLT => 0xA1,
            Instruction::IF_ICMPGE => 0xA2,
            Instruction::IF_ICMPGT => 0xA3,
            Instruction::IF_ICMPLE => 0xA4,
            Instruction::INVOKEVIRTUAL => 0xB6,
            Instruction::IRETURN => 0xAC,
            Instruction::RETURN => 0xB1,
            Instruction::IN => 0xF0,
            Instruction::OUT => 0xF1,
            Instruction::ERR => 0xF2,
            Instruction::HALT => 0xFF,
            Instruction::NOP => 0x00,
        }
    }

    pub fn name(&self) -> &str {
        match self {
            Instruction::BIPUSH => "BIPUSH",
            Instruction::DUP => "DUP",
            Instruction::POP => "POP",
            Instruction::SWAP => "SWAP",
            Instruction::ILOAD => "ILOAD",
            Instruction::ISTORE => "ISTORE",
            Instruction::LDC => "LDC",
            Instruction::IADD => "IADD",
            Instruction::ISUB => "ISUB",
            Instruction::IINC => "IINC",
            Instruction::IAND => "IAND",
            Instruction::IOR => "IOR",
            Instruction::GOTO => "GOTO",
            Instruction::IFEQ => "IFEQ",
            Instruction::IFNE => "IFNE",
            Instruction::IFLT => "IFLT",
            Instruction::IFGT => "IFGT",
            Instruction::IF_ICMPEQ => "IF_ICMPEQ",
            Instruction::IF_ICMPNE => "IF_ICMPNE",
            Instruction::IF_ICMPLT => "IF_ICMPLT",
            Instruction::IF_ICMPGE => "IF_ICMPGE",
            Instruction::IF_ICMPGT => "IF_ICMPGT",
            Instruction::IF_ICMPLE => "IF_ICMPLE",
            Instruction::INVOKEVIRTUAL => "INVOKEVIRTUAL",
            Instruction::IRETURN => "IRETURN",
            Instruction::RETURN => "RETURN",
            Instruction::IN => "IN",
            Instruction::OUT => "OUT",
            Instruction::ERR => "ERR",
            Instruction::HALT => "HALT",
            Instruction::NOP => "NOP",
        }
    }

    pub fn argument_count(&self) -> u8 {
        match self {
            Instruction::BIPUSH => 1,
            Instruction::ILOAD => 1,
            Instruction::ISTORE => 1,
            Instruction::LDC => 1,
            Instruction::IINC => 2,
            Instruction::GOTO => 1,
            Instruction::IFEQ => 1,
            Instruction::IFNE => 1,
            Instruction::IFLT => 1,
            Instruction::IFGT => 1,
            Instruction::IF_ICMPEQ => 1,
            Instruction::IF_ICMPNE => 1,
            Instruction::IF_ICMPLT => 1,
            Instruction::IF_ICMPGE => 1,
            Instruction::IF_ICMPGT => 1,
            Instruction::IF_ICMPLE => 1,
            Instruction::INVOKEVIRTUAL => 1,
            Instruction::OUT => 1,
            Instruction::ERR => 1,
            _ => 0,
        }
    }

    pub fn byte_size(&self) -> u8 {
        // TODO Replace with correct values once new sizes are implemented
        match self {
            Instruction::BIPUSH => 3,
            Instruction::ILOAD => 2,
            Instruction::ISTORE => 2,
            Instruction::LDC => 2,
            Instruction::IINC => 4,
            Instruction::GOTO => 3,
            Instruction::IFEQ => 3,
            Instruction::IFNE => 3,
            Instruction::IFLT => 3,
            Instruction::IFGT => 3,
            Instruction::IF_ICMPEQ => 3,
            Instruction::IF_ICMPNE => 3,
            Instruction::IF_ICMPLT => 3,
            Instruction::IF_ICMPGE => 3,
            Instruction::IF_ICMPGT => 3,
            Instruction::IF_ICMPLE => 3,
            Instruction::INVOKEVIRTUAL => 2,
            Instruction::OUT => 2,
            Instruction::ERR => 2,
            _ => 1,
        }
    }

    pub fn parse_arguments(self: &Instruction, contents: &[u8]) -> Result<Vec<Argument>> {
        match self {
            Instruction::BIPUSH => Ok(vec![extract_literal(contents)?]),
            Instruction::ILOAD => Ok(vec![extract_index(contents)?]),
            Instruction::ISTORE => Ok(vec![extract_index(contents)?]),
            Instruction::LDC => Ok(vec![extract_index(contents)?]),
            Instruction::IINC => {
                let index = extract_index(contents)?;
                let literal = extract_literal(&contents[1..])?;
                Ok(vec![index, literal])
            }
            Instruction::GOTO => Ok(vec![extract_offset(contents)?]),
            Instruction::IFEQ => Ok(vec![extract_offset(contents)?]),
            Instruction::IFNE => Ok(vec![extract_offset(contents)?]),
            Instruction::IFLT => Ok(vec![extract_offset(contents)?]),
            Instruction::IFGT => Ok(vec![extract_offset(contents)?]),
            Instruction::IF_ICMPEQ => Ok(vec![extract_offset(contents)?]),
            Instruction::IF_ICMPNE => Ok(vec![extract_offset(contents)?]),
            Instruction::IF_ICMPLT => Ok(vec![extract_offset(contents)?]),
            Instruction::IF_ICMPGE => Ok(vec![extract_offset(contents)?]),
            Instruction::IF_ICMPGT => Ok(vec![extract_offset(contents)?]),
            Instruction::IF_ICMPLE => Ok(vec![extract_offset(contents)?]),
            Instruction::INVOKEVIRTUAL => Ok(vec![extract_index(contents)?]),
            Instruction::OUT => Ok(vec![extract_index(contents)?]),
            Instruction::ERR => Ok(vec![extract_index(contents)?]),
            _ => Ok(vec![]),
        }
    }

    pub fn from_bytecode(bytecode: u8) -> Option<Self> {
        match bytecode {
            0x10 => Some(Instruction::BIPUSH),
            0x59 => Some(Instruction::DUP),
            0x57 => Some(Instruction::POP),
            0x5F => Some(Instruction::SWAP),
            0x15 => Some(Instruction::ILOAD),
            0x36 => Some(Instruction::ISTORE),
            0x12 => Some(Instruction::LDC),
            0x60 => Some(Instruction::IADD),
            0x64 => Some(Instruction::ISUB),
            0x84 => Some(Instruction::IINC),
            0x7E => Some(Instruction::IAND),
            0x80 => Some(Instruction::IOR),
            0xA7 => Some(Instruction::GOTO),
            0x99 => Some(Instruction::IFEQ),
            0x9A => Some(Instruction::IFNE),
            0x9B => Some(Instruction::IFLT),
            0x9D => Some(Instruction::IFGT),
            0x9F => Some(Instruction::IF_ICMPEQ),
            0xA0 => Some(Instruction::IF_ICMPNE),
            0xA1 => Some(Instruction::IF_ICMPLT),
            0xA2 => Some(Instruction::IF_ICMPGE),
            0xA3 => Some(Instruction::IF_ICMPGT),
            0xA4 => Some(Instruction::IF_ICMPLE),
            0xB6 => Some(Instruction::INVOKEVIRTUAL),
            0xAC => Some(Instruction::IRETURN),
            0xB1 => Some(Instruction::RETURN),
            0xF0 => Some(Instruction::IN),
            0xF1 => Some(Instruction::OUT),
            0xF2 => Some(Instruction::ERR),
            0xFF => Some(Instruction::HALT),
            0x00 => Some(Instruction::NOP),
            _ => None,
        }
    }
}

fn extract_index(contents: &[u8]) -> Result<Argument> {
    // TODO Change result type once new header is ready.
    if contents.is_empty() {
        return Err(ProgramError::InstructionError {
            msg: "Not enough bytes to extract index argument".into(),
        });
    }
    Ok(Argument::INDEX { index: contents[0] })
}

fn extract_offset(contents: &[u8]) -> Result<Argument> {
    // TODO Change result type once new header is ready.
    if contents.len() < ARG_OFFSET_SIZE {
        return Err(ProgramError::InstructionError {
            msg: "Not enough bytes to extract offset argument".into(),
        });
    }
    let offset = get_u16_be(contents, 0)? as i16;
    Ok(Argument::OFFSET { offset })
}

fn extract_literal(contents: &[u8]) -> Result<Argument> {
    // TODO Change result type once new header is ready.
    if contents.len() < ARG_OFFSET_SIZE {
        return Err(ProgramError::InstructionError {
            msg: "Not enough bytes to extract literal argument".into(),
        });
    }
    let value = get_u16_be(contents, 0)? as i16;
    Ok(Argument::LITERAL { value })
}

// TODO Change byte sizes once new header is ready
pub const ARG_INDEX_SIZE: usize = 1;
pub const ARG_OFFSET_SIZE: usize = 2;
pub const ARG_LITERAL_SIZE: usize = 2;

pub enum Argument {
    // TODO Change types once new header is ready
    INDEX { index: u8 },
    OFFSET { offset: i16 },
    LITERAL { value: i16 },
}

impl Argument {
    pub fn byte_size(&self) -> usize {
        match self {
            #[allow(unused_variables)]
            Argument::INDEX { index } => ARG_INDEX_SIZE,
            #[allow(unused_variables)]
            Argument::OFFSET { offset } => ARG_OFFSET_SIZE,
            #[allow(unused_variables)]
            Argument::LITERAL { value } => ARG_LITERAL_SIZE,
        }
    }

    pub fn target_address(&self, ip: usize) -> Option<usize> {
        match self {
            Argument::OFFSET { offset } => {
                Some(ip + 1 + *offset as usize)
            },
            _ => None
        }
    }
}

impl Display for Argument {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Argument::INDEX { index } => write!(f, "{}", index),
            Argument::OFFSET { offset } => write!(f, "{}", offset),
            Argument::LITERAL { value } => write!(
                f,
                "{} ({})",
                value,
                char::from_u32(*value as u32).unwrap_or('□')
            ),
        }
    }
}
