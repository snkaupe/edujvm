use std::fmt::Display;

/// This struct allows its user to analyse the eduJVM version byte
/// found in an eduJVM executable file.
#[derive(Debug)]
pub struct EdujvmVersion {
    version: u8,
}

impl EdujvmVersion {
    pub fn new(version: u8) -> Self {
        EdujvmVersion { version }
    }

    /// Get the version byte.
    pub fn version(&self) -> u8 {
        self.version
    }

    /// Get the version's major component.
    pub fn major(&self) -> u8 {
        self.version >> 4
    }

    /// Get the version's minor component.
    pub fn minor(&self) -> u8 {
        self.version & 0x0F
    }
}

impl Display for EdujvmVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}.{}", self.major(), self.minor())
    }
}
