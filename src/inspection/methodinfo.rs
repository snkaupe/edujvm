use super::util::{get_u16_be, get_u32_be};
use super::Result;
use super::{Program, ProgramError};

#[derive(Debug)]
pub struct MethodInfo {
    index: u16,
    address: usize,
    parameter_count: u8,
    local_variable_count: u8,
    debug_info_address: Option<usize>,
    debug_info: Option<DebugInformation>,
}

impl MethodInfo {
    pub fn from_bytes(index: u16, bytes: &[u8]) -> Result<Self> {
        let maddr = get_u32_be(bytes, 0)? as usize;
        let pcnt = bytes[4];
        let lcnt = bytes[5];
        let dbgaddr = get_u32_be(bytes, 6)? as usize;
        Ok(Self {
            index,
            address: maddr,
            parameter_count: pcnt,
            local_variable_count: lcnt,
            debug_info_address: if dbgaddr > 0 { Some(dbgaddr) } else { None },
            debug_info: None,
        })
    }

    pub fn from_bytes_complete(index: u16, bytes: &[u8]) -> Result<Self> {
        let num_methods = bytes[10] as usize;
        if index > num_methods as u16 {
            return Err(ProgramError::InvalidIndex {
                index,
                max: num_methods as u16,
            });
        }
        let offset = 13 + (index as usize * 10);
        let mut method_info = MethodInfo::from_bytes(index, &bytes[offset..offset + 10])?;
        if method_info.has_debug_info() {
            let lower = method_info.debug_info_address().unwrap();
            method_info.debug_info = Some(DebugInformation::from_bytes(&bytes[lower..])?);
        }
        Ok(method_info)
    }

    pub fn from_program(index: u16, program: &Program) -> Result<Self> {
        MethodInfo::from_bytes_complete(index, program.bytes())
    }

    /// Get the MethodInfo's index.
    pub fn index(&self) -> u16 {
        self.index
    }

    pub fn address(&self) -> usize {
        self.address
    }

    pub fn parameter_count(&self) -> u8 {
        self.parameter_count
    }

    pub fn local_variable_count(&self) -> u8 {
        self.local_variable_count
    }

    pub fn debug_info_address(&self) -> Option<usize> {
        self.debug_info_address
    }

    pub fn has_debug_info(&self) -> bool {
        self.debug_info_address.is_some()
    }

    /// Get a reference to the method info's debug info.
    #[must_use]
    pub fn debug_info(&self) -> Option<&DebugInformation> {
        self.debug_info.as_ref()
    }
}

#[derive(Debug)]
pub struct DebugInformation {
    name_size: u16,
    compiled_size: u16,
    name: String,
    parameter_names: Vec<String>,
    variable_names: Vec<String>,
}

impl DebugInformation {
    pub fn from_bytes(bytes: &[u8]) -> Result<Self> {
        let name_size = bytes[0] as u16;
        let compiled_size = get_u16_be(bytes, 1)?;
        let name = match String::from_utf8(Vec::from(&bytes[3..3 + name_size as usize])) {
            Ok(name) => name,
            Err(e) => {
                return Err(ProgramError::EncodingError {
                    msg: "method name is not valid UTF-8".into(),
                    cause: Some(e),
                })
            }
        };
        Ok(DebugInformation {
            name_size,
            compiled_size,
            name,
            parameter_names: vec![],
            variable_names: vec![],
        })
    }

    /// Get the debug information's name size.
    #[must_use]
    pub fn name_size(&self) -> u16 {
        self.name_size
    }

    /// Get the debug information's compiled size.
    #[must_use]
    pub fn compiled_size(&self) -> u16 {
        self.compiled_size
    }

    /// Get a reference to the debug information's name.
    #[must_use]
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub fn parameter_count(&self) -> u8 {
        self.parameter_names.len() as u8
    }

    pub fn parameter_name(&self, idx: u8) -> Result<&str> {
        if idx >= self.parameter_count() {
            return Err(ProgramError::InvalidIndex {
                index: idx as u16,
                max: self.parameter_count() as u16,
            });
        }
        Ok(&self.parameter_names[idx as usize])
    }

    pub fn variable_count(&self) -> u8 {
        self.variable_names.len() as u8
    }

    pub fn variable_name(&self, idx: u8) -> Result<&str> {
        if idx >= self.variable_count() {
            return Err(ProgramError::InvalidIndex {
                index: idx as u16,
                max: self.variable_count() as u16,
            });
        }
        Ok(&self.variable_names[idx as usize])
    }
}
