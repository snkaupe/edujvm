use super::{ProgramError, Result};

pub fn get_u16_be(contents: &[u8], offset: usize) -> Result<u16> {
    let res = contents[offset..offset + 2].try_into();
    match res {
        Ok(arr) => Ok(u16::from_be_bytes(arr)),
        Err(e) => Err(ProgramError::ConversionError {
            msg: format!("unable to get u16: {}", e),
        }),
    }
}

pub fn get_u32_be(contents: &[u8], offset: usize) -> Result<u32> {
    let res = contents[offset..offset + 4].try_into();
    match res {
        Ok(arr) => Ok(u32::from_be_bytes(arr)),
        Err(e) => Err(ProgramError::ConversionError {
            msg: format!("unable to get u32: {}", e),
        }),
    }
}

pub fn get_u16_le(contents: &[u8], offset: usize) -> Result<u16> {
    let res = contents[offset..offset + 2].try_into();
    match res {
        Ok(arr) => Ok(u16::from_le_bytes(arr)),
        Err(e) => Err(ProgramError::ConversionError {
            msg: format!("unable to get u16: {}", e),
        }),
    }
}

pub fn get_u32_le(contents: &[u8], offset: usize) -> Result<u32> {
    let res = contents[offset..offset + 4].try_into();
    match res {
        Ok(arr) => Ok(u32::from_le_bytes(arr)),
        Err(e) => Err(ProgramError::ConversionError {
            msg: format!("unable to get u32: {}", e),
        }),
    }
}
