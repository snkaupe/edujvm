mod edujvmversion;
mod instructions;
mod methodinfo;
mod program;
pub(crate) mod util;

use std::string::FromUtf8Error;

pub use edujvmversion::EdujvmVersion;
pub use instructions::{Argument, Instruction};
pub use methodinfo::{DebugInformation, MethodInfo};
pub use program::Program;

#[derive(Debug)]
pub enum ProgramError {
    NotAValidProgram {
        msg: String,
    },
    InvalidOffset {
        offset: usize,
        max: usize,
        msg: String,
    },
    InvalidIndex {
        index: u16,
        max: u16,
    },
    EncodingError {
        msg: String,
        cause: Option<FromUtf8Error>,
    },
    IoError {
        cause: std::io::Error,
    },
    // TODO Different type?
    ConversionError {
        msg: String,
    },
    InstructionError {
        msg: String,
    },
}

pub type Result<T> = std::result::Result<T, ProgramError>;
