use std::{
    fs::File,
    io::{BufReader, Read},
};

use super::util::get_u16_be;
use super::{EdujvmVersion, MethodInfo, ProgramError, Result};

pub const MAGIC: [u8; 4] = [0x65, 0x4A, 0x56, 0x4D];

#[derive(Debug)]
pub struct Program {
    contents: Vec<u8>,
    name: String,
    version: EdujvmVersion,
    methods: Vec<MethodInfo>,
    constant_count: u16,
    pub addresses: Addresses,
}

impl Program {
    pub fn from_file(file: File) -> Result<Self> {
        let mut reader = BufReader::new(file);
        let mut contents = Vec::with_capacity(16384);
        let read = reader.read_to_end(&mut contents);
        if let Ok(num_bytes) = read {
            // The 13 bytes minimum length comes from the eduJVM language
            // specification. Or, more precisely, the old eJVM spec. This might
            // change later on, should I decide to change the executable file
            // header format.
            // TODO Check once header format is fix.
            if num_bytes <= 13 {
                return Err(ProgramError::NotAValidProgram {
                    msg: format!("Not enough bytes (found {})", num_bytes),
                });
            }
            contents.shrink_to_fit(); // TODO Necessary/useful?
            parse_program(contents)
        } else {
            Err(ProgramError::IoError {
                cause: read.expect_err("Found Ok where Err was expected"),
            })
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn version(&self) -> &EdujvmVersion {
        &self.version
    }

    /// Returns a reference to the complete program's byte representation.
    #[must_use]
    pub fn bytes(&self) -> &[u8] {
        &self.contents
    }

    /// Get a reference to the program's methods.
    pub fn methods(&self) -> &[MethodInfo] {
        self.methods.as_ref()
    }

    /// Get the program's constant count.
    pub fn constant_count(&self) -> u16 {
        self.constant_count
    }

    pub fn constant(&self, index: u16) -> Result<u16> {
        if index >= self.constant_count {
            return Err(ProgramError::InvalidIndex {
                index,
                max: self.constant_count(),
            });
        }
        get_u16_be(
            &self.contents,
            self.addresses.constant_table + index as usize * 2,
        )
    }

    pub fn instructions_for_method(&self, method: &MethodInfo) -> Result<&[u8]> {
        let end = method.address() + method.debug_info().unwrap().compiled_size() as usize;
        if end <= method.address() {
            return Err(ProgramError::InvalidOffset {
                offset: method.address(),
                max: end,
                msg: "Method end address before method start address".into(),
            });
        }
        self.check_text_address(method.address())?;
        self.check_text_address(end)?;
        Ok(&self.contents[method.address()..end])
    }

    fn check_text_address(&self, address: usize) -> Result<()> {
        if self.addresses.debug_info > 0 && address > self.addresses.debug_info {
            return Err(ProgramError::InvalidOffset {
                offset: address,
                max: self.addresses.debug_info,
                msg: "address overflows into debug info pool".into(),
            });
        }
        if self.addresses.error_table > 0 && address > self.addresses.error_table {
            return Err(ProgramError::InvalidOffset {
                offset: address,
                max: self.addresses.error_table,
                msg: "address overflows into error table".into(),
            });
        }
        if address >= self.addresses.name {
            return Err(ProgramError::InvalidOffset {
                offset: address,
                max: self.addresses.name,
                msg: "address overflows into program name".into(),
            });
        }
        Ok(())
    }

    pub fn size(&self) -> usize {
        self.contents.len()
    }

    pub fn is_module(&self) -> bool {
        false // TODO
    }

    pub fn is_program(&self) -> bool {
        !self.is_module()
    }

    pub fn has_debug_info(&self) -> bool {
        self.addresses.debug_info != 0
    }
}

#[derive(Debug)]
pub struct Addresses {
    pub method_table: usize,
    pub constant_table: usize,
    pub error_table: usize,
    pub text_segment: usize,
    pub debug_info: usize,
    pub name: usize,
}

impl Addresses {
    pub fn new(
        method_table: usize,
        constant_table: usize,
        error_table: usize,
        text_segment: usize,
        debug_info: usize,
        name: usize,
    ) -> Self {
        Self {
            method_table,
            constant_table,
            error_table,
            text_segment,
            debug_info,
            name,
        }
    }

    pub fn has_constant_table(&self) -> bool {
        self.constant_table != 0
    }

    pub fn has_error_table(&self) -> bool {
        self.error_table != 0
    }

    pub fn has_debug_info(&self) -> bool {
        self.debug_info != 0
    }
}

fn parse_program(contents: Vec<u8>) -> Result<Program> {
    let magic_slice = &contents[0..4];
    if magic_slice != MAGIC {
        return Err(ProgramError::NotAValidProgram {
            msg: format!(
                "Wrong magic number (expected {:?}, found {:?})",
                MAGIC,
                &contents[0..4]
            ),
        });
    }

    let version = EdujvmVersion::new(contents[4]);
    let name_offset =
        u32::from_be_bytes([contents[6], contents[7], contents[8], contents[9]]) as usize;
    let name = {
        // TODO Use little endian throughout?
        let name_end = name_offset + contents[5] as usize;
        // TODO Use UTF-8 for name encoding in the language spec
        match String::from_utf8(Vec::from(&contents[name_offset..name_end])) {
            Ok(name) => name,
            Err(e) => {
                return Err(ProgramError::EncodingError {
                    msg: "name is not valid UTF-8".into(),
                    cause: Some(e),
                })
            }
        }
    };

    let methods = extract_method_table(&contents)?;

    // TODO Future version for new header:
    // let constant_count = get_u16_be(&contents, 11)?;
    let constant_cnt = contents[11] as u16;
    let constant_table_offset = 13 + (methods.len() * 10);

    let text_offset = methods.get(0).map_or(0, |m| m.address());

    let debug_info_offset = methods
        .get(0)
        .and_then(|m| m.debug_info_address())
        .unwrap_or(0);

    let error_table_offset = {
        if contents[12] == 0 || methods.is_empty() {
            // TODO Can the method list even be empty?
            0usize
        } else if let Some(last_dbg_entry_start) = methods.last().unwrap().debug_info_address() {
            // Add to the address the three bytes always present and the length of the method's name
            last_dbg_entry_start + 3 + contents[last_dbg_entry_start] as usize
        } else {
            // No debug info available
            // TODO This should be text_offset + length of text segment
            0usize
        }
    };

    Ok(Program {
        contents,
        name,
        version,
        methods,
        constant_count: constant_cnt,
        addresses: Addresses::new(
            13,
            constant_table_offset,
            error_table_offset,
            text_offset,
            debug_info_offset,
            name_offset,
        ),
    })
}

fn extract_method_table(contents: &[u8]) -> Result<Vec<MethodInfo>> {
    let num_methods = contents[10] as usize;
    let mut method_table = Vec::with_capacity(num_methods);
    let mut index = 0u16;
    while (index as usize) < num_methods {
        method_table.push(MethodInfo::from_bytes_complete(index, contents)?);
        index += 1;
    }
    Ok(method_table)
}
