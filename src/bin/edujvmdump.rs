use std::fmt::Write;
use std::fs::File;

use clap::Parser;
use const_format::formatcp;
use edujvm::inspection::*;

#[derive(Parser, Debug)]
#[clap(name = "edujvmdump", version = formatcp!("\neduJVM Version {} (crate version {})", edujvm::EDUJVM_VERSION, edujvm::CRATE_VERSION), author, about = "Disassembler for eduJVM executable files (.exf) and module files (.emf).", long_about = None)]
struct Args {
    #[clap()]
    path: String,
    #[clap(long, help = "Removes type prefixes from the output")]
    no_type_prefixes: bool,
    #[clap(long, help = "Do not replace method table index of INVOKEVIRTUAL")]
    no_method_name_substitution: bool,
    #[clap(long, help = "Do not replace jump offsets with target addresses")]
    no_offset_substitution: bool,
}

fn main() {
    let args = Args::parse();
    let file = File::open(&args.path);
    match file {
        Ok(f) => {
            let program = Program::from_file(f);
            if let Ok(p) = program {
                if let Err(cause) = process_program(p) {
                    println!("An error occured while analyzing the program: {:?}", cause);
                    // TODO Improve output
                }
            } else {
                println!(
                    "An error occured processing the program file: {:?}",
                    program.unwrap_err()
                );
            }
        }
        Err(e) => println!("An error occured opening the file: {}", e),
    }
}

fn process_program(program: Program) -> Result<()> {
    println!(
        "{} name: {} ({} bytes at address {:>#010X})",
        if program.is_program() {
            "Program"
        } else {
            "Module"
        },
        program.name(),
        program.name().len(),
        program.addresses.name,
    );
    println!("Required eduJVM language version: {}", &program.version());
    // TODO Remove
    println!("Addresses: {:?}", program.addresses);
    dump_method_table(&program)?;
    dump_constants(&program)?;
    dump_methods(&program)?;
    Ok(())
}

fn dump_method_table(program: &Program) -> Result<()> {
    println!("Method table ({:>#010X}):", program.addresses.method_table);
    let max_name_width = {
        let method = program
            .methods()
            .iter()
            .max_by_key(|m| m.debug_info().unwrap().name().chars().count());
        // This should be safe, as the max_by_key method wouldn't have returned it otherwise
        method.unwrap().debug_info().unwrap().name().chars().count()
    };
    for method in program.methods() {
        println!(
            "  {:>3}: {:>max_name_width$}@{:#010X}   {:>3} parameters   {:>3} local variables   {:>5} bytes length",
            method.index(),
            method.debug_info().map(|dbg| dbg.name()).unwrap_or("???"), // TODO Check if we can come up with a better replacement
            method.address(),
            method.parameter_count(),
            method.local_variable_count(),
            method.debug_info().unwrap().compiled_size()
        );
    }
    Ok(())
}

fn dump_constants(program: &Program) -> Result<()> {
    println!(
        "Constant table ({:>#010X}):",
        program.addresses.constant_table
    );
    let mut current_index = 0;
    while current_index < program.constant_count() {
        let value = program
            .constant(current_index)
            .expect("constant index out of bounds");
        println!("  {:>3}: {} ({:#010X})", current_index, value, value);
        current_index += 1;
    }
    Ok(())
}

fn dump_methods(program: &Program) -> Result<()> {
    println!("Method dumps:");
    for method in program.methods() {
        let instructions = program.instructions_for_method(method)?;
        let mut current_offset = 0;
        let mut cnt = 0;
        // TODO Improve method name replacement when no debug info is present
        println!(
            "  {}():",
            method.debug_info().map(|dbg| dbg.name()).unwrap_or("???")
        );
        while current_offset < method.debug_info().unwrap().compiled_size() as usize {
            if let Some(instruction) = Instruction::from_bytecode(instructions[current_offset]) {
                let instruction_bytes = &instructions
                    [current_offset..current_offset + instruction.byte_size() as usize];
                print!(
                    "    {:>4} ({:>#010X}):   ",
                    cnt,
                    method.address() + current_offset,
                );
                print!(
                    " {:<20}",
                    create_byte_string(&instruction, instruction_bytes)?
                );
                print!("  {}", instruction.name());
                for argument in instruction.parse_arguments(&instruction_bytes[1..])? {
                    match argument {
                        Argument::INDEX { index } => print!(" {}", index),
                        Argument::OFFSET { offset } => print!(
                            " {:>#010X}",
                            // TODO The old eJVM implementation seems to take the bytes for the instruction and its
                            // arguments into account when calculating jump addresses. For this reason, we add the
                            //  byte size of the instruction to our "instruction pointer" (current_offset).
                            method.address()
                                + instruction.byte_size() as usize
                                + (current_offset as i16 + offset) as usize
                        ),
                        Argument::LITERAL { value } => print!(
                            " {} ('{}')",
                            value,
                            char::from_u32(value as u32).unwrap_or('□')
                        ),
                    }
                }
                println!();
                cnt += 1;
                current_offset += instruction.byte_size() as usize;
            } else {
                return Err(ProgramError::InstructionError {
                    msg: format!(
                        "Unknown bytecode: {} @ {:#>010X}",
                        instructions[current_offset],
                        method.address() + current_offset
                    ),
                });
            }
        }
    }
    Ok(())
}

fn create_byte_string(instruction: &Instruction, contents: &[u8]) -> Result<String> {
    // TODO Calculate proper capacity once the new header is in place.
    let mut result = String::with_capacity(10);
    if instruction.byte_size() as usize != contents.len() {
        return Err(ProgramError::InstructionError {
            msg: format!(
                "Missing bytes for instruction {}. Expected {}, got {}",
                instruction.name(),
                instruction.byte_size(),
                contents.len()
            ),
        });
    }
    for byte in contents {
        write!(result, "{:>#04X} ", byte).expect("Error formatting instruction bytes string");
    }
    Ok(result)
}
