use clap::Parser;
use const_format::formatcp;

#[derive(Parser, Debug)]
#[clap(name = "edujvm", version = formatcp!("\neduJVM Version {} (crate version {})", edujvm::EDUJVM_VERSION, edujvm::CRATE_VERSION), author, about = "Virtual machine for running eduJVM executable files (.exf).", long_about = None)]
struct Args {
    #[clap()]
    path: String,
}
fn main() {
    let args = Args::parse();
    println!("args: {:?}", args);
}
