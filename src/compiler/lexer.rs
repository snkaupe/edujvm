use super::{LexerError, Result};

#[derive(Debug)]
pub enum Directives {
    Program,
    Module,
    RequiredVersion,
    Exports,
    EndExports,
    Constants,
    EndConstants,
    Errors,
    EndErrors,
    Method,
    EndMethod,
    Vars,
    EndVars,
}

#[derive(Debug)]
pub enum Literals {
    HexInt,
    BinInt,
    Int,
    Char,
    String,
}

#[derive(Debug)]
pub enum TokenType {
    Directive(Directives),
    ObjectName,
    Literal(Literals),
    Identifier,
    Instruction,
}

#[derive(Debug)]
pub struct Token {
    line: u32,
    char: u32,
    text: String,
    token_type: TokenType,
}

impl Token {
    /// Get the line the token was found on.
    pub fn line(&self) -> u32 {
        self.line
    }

    /// Get the position (in characters) of the token's start in the line.
    pub fn char(&self) -> u32 {
        self.char
    }

    /// Get a reference to the token's text.
    pub fn text(&self) -> &str {
        self.text.as_ref()
    }

    /// Get a reference to the token's TokenType.
    pub fn token_type(&self) -> &TokenType {
        &self.token_type
    }
}

#[derive(Debug)]
pub struct Lexer {}
