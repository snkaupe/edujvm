mod lexer;

pub use lexer::{Directives, Lexer, Literals, Token, TokenType};

#[derive(Debug)]
pub enum LexerError {
    UnexpectedSymbol {
        line: u32,
        char: u32,
        expected: String,
        found: String,
    },
    UnexpectedToken {
        line: u32,
        char: u32,
        expected: TokenType,
        found: Token,
    },
}

#[derive(Debug)]
pub enum CompileError {
    LexerError { cause: LexerError },
}

impl From<LexerError> for CompileError {
    fn from(le: LexerError) -> Self {
        CompileError::LexerError { cause: le }
    }
}

pub type Result<T> = std::result::Result<T, CompileError>;
