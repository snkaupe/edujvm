; A module is a library that exports functionality for the use in other
; modules or an eduJVM program.
.module Math

.version 0x01

; Only methods found in this table are available for being called
; from outside the module.
.exports
    divide
.end-exports

; Depending on what I end up doing with error handling, errors might
; also be exported automatically or if they appear in the exports table.
.errors
    E_DIV_BY_ZERO    "Division by zero!"
.end-errors

.method divide(dividend, divisor)
    .vars
        cnt
    .end-vars
            ILOAD divisor           ; Check for division by zero.
            IFEQ err                ; Go to err if dividend is 0.
    sub:    ILOAD divisor           ; Place divisor and...
            ILOAD dividend          ; ...dividend on the stack.
            ISUB                    ; Subtract divisor from current value.
            DUP                     ; Duplicate value.
            IFLT end                ; We are done once we are below 0.
            ISTORE dividend         ; Write new value into local variable.
            IINC cnt 1              ; Increment subtraction counter.
            GOTO sub                ; Next iteration.
    end:    ILOAD cnt               ; Put cnt on stack.
            IRETURN                 ; Return top of stack as result.
    err:    ERR E_DIV_BY_ZERO       ; Abort with error message.
.end-method